<?php $title = 'Accueil' ?>
 
<?php ob_start() ?>
  <div class="carousel-container visible-lg">
    <div id="carousel">
      <div class="carousel-feature">
        <a href="#"><img class="carousel-image" alt="Image Caption" src="public/images/company1.jpg"></a>
        <div class="carousel-caption">
          <p>
            Une entreprise innovante au coeur de Lyon.
          </p>
        </div>
      </div>
      <div class="carousel-feature">
        <a href="#"><img class="carousel-image" alt="Image Caption" src="public/images/company2.jpg"></a>
        <div class="carousel-caption">
          <p>
            Venez découvrir nos locaux!
          </p>
        </div>
      </div>
      <div class="carousel-feature">
        <a href="#"><img class="carousel-image" alt="Image Caption" src="public/images/company3.jpg"></a>
        <div class="carousel-caption">
          <p>
            Des hommes et des femmes au service du futur.
          </p>
        </div>
      </div>
      <div class="carousel-feature">
        <a href="#"><img class="carousel-image" alt="Image Caption" src="public/images/company4.jpg"></a>
        <div class="carousel-caption">
          <p>
            La robotique, plus qu'un métier : une passion
          </p>
        </div>
      </div>
      <div class="carousel-feature">
        <a href="#"><img class="carousel-image" alt="Image Caption" src="public/images/company5.jpg"></a>
        <div class="carousel-caption">
          <p>
            Des prototypes incroyables à découvrir.
          </p>
        </div>
      </div>
    </div>
  
    <div id="carousel-left"><img src="public/images/arrow-left.png" alt="left arrow" /></div>
    <div id="carousel-right"><img src="public/images/arrow-right.png" alt="right arrow" /></div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      var carousel = $("#carousel").featureCarousel({
        // include options like this:
        // (use quotes only for string values, and no trailing comma after last option)
        // option: value,
        // option: value
      });
    });
  </script>

  <div class="row">
    <div class='col-md-4'>
      <h2>A propos</h2>
      <img src="./public/images/aboutus.jpg" alt="Erreur chargement de l'image : about us écrit sur des cubes">
      <p>Lorem ipsum vestibulum augue, sed vulputate felis. Pellentesque ut odio feugiat, venenatis lorem nec, euismod felis. Nam placerat in nulla interdum rhoncus. Suspendisse in tellus vulputate libero auctor vehicula. Duis condimentum justo ut lectus consequat pellentesque. In aliquam leo ultricies odio imperdiet tristique.</p>
      <p>Vivamus ipsum magna, tristique a gravida eu, convallis et mauris. Aenean lobortis condimentum gravida. Nulla ac bibendum neque. Vestibulum ac varius quam. In nec semper diam. Nullam ac tempor lectus, ac elementum elit. Cras sollicitudin lacus mi, in interdum tortor hendrerit at. Aenean ac orci vel ligula ornare suscipit. Integer congue rhoncus elementum. Cras massa nisi, convallis porta rhoncus sit amet, pretium et turpis. Integer vestibulum diam tortor, sed tempus quam malesuada vitae. Etiam vulputate, enim in cursus sollicitudin, libero magna aliquet nisi, et porta risus nibh id ipsum. In varius enim non diam ultricies, non porttitor elit ultricies. Proin commodo sagittis velit, nec accumsan felis laoreet id. In elementum mi sit amet nunc feugiat, ut rhoncus odio porttitor. Vivamus consectetur eu libero et tincidunt.</p>
    </div>
    <div class='col-md-4'>
      <h2>Sponsors</h2>
      <img src="./public/images/sponsor1.jpg" alt="Erreur chargement de l'image : Logo Sony">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, impedit, totam, repudiandae, laboriosam doloribus provident recusandae aspernatur quos eum odit alias cum? Sint, modi, temporibus exercitationem iste animi commodi porro eum iusto nulla dicta ab earum perferendis. At, accusantium praesentium blanditiis in adipisci nesciunt iusto id libero dolore dolorum culpa natus nemo molestias. Quod atque quo nemo quae eaque dolores impedit quisquam expedita voluptatibus. Fugiat, rem odio amet quas voluptatum eaque voluptatibus porro. Fugiat, ab vero repellendus mollitia porro quidem nostrum adipisci facere suscipit ad reiciendis nesciunt veniam est.</p>
      <br><br>
      <img src="./public/images/sponsor2.jpg" alt="Erreur chargement de l'image : Logo IBM">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, omnis, voluptas dolorum nisi eos corporis beatae dolor natus a nostrum ratione deleniti placeat similique quis ullam ipsum praesentium reprehenderit eum consectetur mollitia ex nam velit vitae earum pariatur ducimus illo. Reiciendis, corrupti, omnis libero itaque possimus dicta quo aut voluptate nulla voluptas quas illum optio inventore eius laborum a modi veritatis id vero facere voluptatum in consequuntur quae culpa blanditiis.</p>
    </div>
    <div class='col-md-4'>
      <h2>Contact</h2>
      <img src="./public/images/contact.png" alt="Erreur chargement de l'image : Femme avec un micro-casque">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, nesciunt, aut quibusdam tempora numquam nulla libero. Quos, iste, dolorum, laborum, architecto at nemo ea qui laudantium ipsa ad minima perferendis amet? Soluta, et, blanditiis sequi ducimus nobis nesciunt harum possimus nisi. Eligendi, alias ducimus explicabo in ex molestias sapiente expedita voluptatum obcaecati repellat delectus repudiandae numquam. Similique distinctio aperiam sed neque sint. At, porro corrupti!</p>
    </div>
  </div>

<?php $content = ob_get_clean(); ?>

<?php include 'layout.php' ?>

