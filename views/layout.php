<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=false">
        <title><?php echo $title ?> | Robots With Me</title>
        <link rel="stylesheet" type="text/css" href="./public/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="./public/css/caroussel-style.css" />
        <link rel="stylesheet" type="text/css" href="./public/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="./public/css/jquery.pnotify.default.css" />
        <link rel="icon" href="./public/images/favicon.png" />
        <script src="./public/js/jquery-1.7.min.js" type="text/javascript"></script>
        <script src="./public/js/jquery.featureCarousel.min.js" type="text/javascript"></script>
        <script src="./public/js/cart.js" type="text/javascript"></script>
        <script src="./public/js/nav.js" type="text/javascript"></script>
        <script src="./public/js/jquery.pnotify.min.js" type="text/javascript"></script>
        <script src="./public/js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="wrapper">
            <header>
                <h1>Robot With Me</h1>
                <div class="picture hidden-xs"></div>
            </header>
            <nav id="nav" class="navbar navbar-default" role="navigation">
             <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" 
                     data-target="#navbar-collapse">
                     <img src="./public/images/arrow-down.png" alt="display-menu">
                  </button>
                  <span class="navbar-brand visible-xs">Navigation</span>
               </div>
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li <?php echo get_current_class("home", $title); ?>>
                            <a href="/" >Accueil</a>
                        </li>
                        <li <?php echo get_current_class("about", $title); ?>>
                            <a href="about.php">A propos</a>
                        </li>
                        <li <?php echo get_current_class("products", $title); ?>>
                            <a href="products.php" >Nos produits</a>
                        </li>
                        <li <?php echo get_current_class("inscription", $title); ?>>
                            <a href="inscription.php" >Inscription</a>
                        <li <?php echo get_current_class("cart", $title); ?>>
                            <a href="cart.php">Panier</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="content <?php echo strtolower($title); ?>">
                <?php echo $content ?>
            </div>
            <footer>
                Robot With Me - entreprise au capital de 10000€. Co-gérants Jéremy Longin et Baptiste Bouchereau 
            </footer>
        </div>
    </body>
</html>
