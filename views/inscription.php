<?php $title = 'Inscription' ?>

<?php ob_start() ?>
	<h2>Inscription</h2>
	<div class="inscription">
		<form method='post' action='inscription.php'>
			<fieldset>
				<label id="name">Nom</label>
				<input class="formInput" type="text" name="lastname" />
			</fieldset>
			<fieldset>
				<label id="lastname">Prénom</label>
				<input class="formInput" type="text" name="firstname" />
			</fieldset>
			<fieldset>
				<label id="email">Email</label>
				<input class="formInput" type="email" name="email" />
			</fieldset>
			<fieldset>
				<label id="age">Age</label>
				<input class="formInput" type="number" name="age" min="1" max="100">
			</fieldset>
		<button type="submit">Envoyer</button>
		</form>
	</div>
	<p class="result <?php echo $result['error_type']?>">
	    <?php echo $result['message']?>
	</p>
<?php $content = ob_get_clean(); ?>

<?php include 'layout.php' ?>

