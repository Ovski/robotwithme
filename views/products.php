<?php $title = 'Produits' ?>

<?php ob_start() ?>
    <h2>Nos produits</h2>
    <table class="table">
        <thead>
            <tr>
                <td class="hidden-xs">Numéro de série</td>
                <td>Titre</td>
                <td>Prix</td>
                <td colspan="3">Disponibilité</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($robots as $robot): ?>
            <tr>
                <td class="hidden-xs"><?php echo $robot['id']?></td>
                <td><?php echo $robot['title']?></td>
                <td><?php echo $robot['price']?></td>
                <td id="number-<?php echo $robot['id']?>"><?php echo $robot['number']?></td>
                <td class="hidden-xs">
                    <img
                        width="150"
                        height="150"
                        src="<?php echo get_robot_photo_url($robot['imgbase64']); ?>"
                        alt="<?php echo $robot['title']?>"
                    />
                </td>
                <td>
                    <div class="add-to-cart-<?php echo  $robot['id']?>">
                        <button
                            class="add-to-cart small"
                            data-id="<?php echo  $robot['id']?>"
                            data-title="<?php echo $robot['title']?>"
                            data-price="<?php echo $robot['price']?>"
                        >
                            Ajouter au panier
                        </button>
                    </div>
                    <div class="div-pm-to-cart">
                        <div class="div-pm-to-cart-<?php echo  $robot['id']?>">
                            <button
                                class="minus-from-cart small"
                                data-id="<?php echo  $robot['id']?>"
                                data-title="<?php echo $robot['title']?>"
                                data-price="<?php echo $robot['price']?>"
                            >
                                -
                            </button>
                            <span class="quantity-in-cart quantity-in-cart-<?php echo  $robot['id']?>"></span>
                            <button
                                class="plus-to-cart small"
                                data-id="<?php echo  $robot['id']?>"
                                data-title="<?php echo $robot['title']?>"
                                data-price="<?php echo $robot['price']?>"
                            >
                                +
                            </button>
                        </div>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
        
<?php $content = ob_get_clean(); ?>

<?php include 'layout.php' ?>

