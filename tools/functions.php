<?php

    /**
     * Get the current class if the link match the title
     *
     * @param string link : the link value
     * @param string title : the page title
     * @return string : the current class or nothing
     */
    function get_current_class($link, $title)
    {
        if ($title == "Accueil" && $link == "home" || 
            $title == "A propos" && $link == "about" ||
            $title == "Inscription" && $link == "inscription" ||
            $title == "Produits" && $link == "products" ||
            $title == "Panier" && $link == "cart"
        ) {
            return "class=\"current\"";
        }
        return null;
    }

    /**
     * Get the url of a robot photo
     *
     * @param string base64 : the image encoded in base64
     * @return string : a well formed url
     */
    function get_robot_photo_url($base64)
    {
        if ($base64 != null) {
            return sprintf("data:image/jpeg;base64,%s", $base64);
        }
        return "/public/images/missing-photo.jpg";
    }
?>
