<?php
	require './tools/functions.php';
	require_once './model/model.php';

    $result = '';

    // no blank fields
	if(isset( $_POST['lastname'])
		&& !empty($_POST['firstname'])
		&& !empty($_POST['lastname'])
		&& !empty($_POST['age'])
		&& !empty($_POST['email']))
	{
	    // email validation
	    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $result = array(
		        'error_type' => 'error',
		        'message'    => 'Votre email est invalide'
		    );
		// age validation
        } else if (!is_numeric($_POST['age']) || $_POST['age'] < 0 || $_POST['age'] > 100) {
            $result = array(
		        'error_type' => 'error',
		        'message'    => 'Votre âge est invalide'
		   );
		// check if the user already exists
        } else if (!user_exists($_POST['email'])) {
	        insert_user(
			    trim ( $_POST['firstname'] ),
			    trim ( $_POST['lastname'] ),
			    trim ( $_POST['age'] ),
			    trim ( $_POST['email'] )
		    );
		    $result = array(
		        'error_type' => 'success',
		        'message'    => 'Vous êtes correctement inscrit.'
		    );
		} else {
		    $result = array(
		        'error_type' => 'error',
		        'message'    => 'L\'utilisateur '.$_POST['email'].' est déjà enregistré.'
		    );
		}
	}

	$robots = get_all_robots();

	require './views/inscription.php';
?>
