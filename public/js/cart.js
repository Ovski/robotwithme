jQuery('document').ready(function() {

    /*
        JSON STRUCTURE
        {
            [
                { "id": x, "number": x, ... },
                { "id": x, "number": x, ... }
            ]
        }
    */

    /**
     * Get cart
     */
    Storage.prototype.getCart = function() {
        var jsonObjectCart = jQuery.parseJSON(localStorage.getItem('cart'));
        return jsonObjectCart;
    };

    /**
     * Set cart
     */
    Storage.prototype.setCart = function(jsonObjectCart) {
        var cart = JSON.stringify(jsonObjectCart);
        localStorage.setItem('cart', cart);
    };

    /**
     * Add product to cart
     */
    Storage.prototype.addProductToCart = function(product) {
        var cart = localStorage.getCart();
        var insert = true;
        // increment number if already in cart
        for(i=0; i<cart.length; i++) {
            if (cart[i].id == product.id) {
                cart[i].number++;
                localStorage.setCart(cart);
                //console.log(localStorage.getCart());
                return;
            }
        }
        product.number = 1;
        cart.push(product);
        localStorage.setCart(cart);
        //console.log(localStorage.getCart());
    };

    /**
     * Remove product from cart
     */
    Storage.prototype.removeProductFromCart = function(id) {
        var cart = localStorage.getCart();
        for(var key in cart) {
            if (cart[key].id == id) {
                if (cart[key].number == 1) {
                    cart.splice(key, 1);
                } else {
                    cart[key].number--;
                }
                localStorage.setCart(cart);
                return;
            }
        }
    };

    /**
     * Get product number in cart
     */
    Storage.prototype.getProductNumberInCart = function(productId) {
        var cart = localStorage.getCart();
        for(i=0; i<cart.length; i++) {
            if (cart[i].id == productId) {
                return cart[i].number;
            }
        }
        return 0;
    };

    /* Initialized the cart variable on document ready */
    var cart;
    if (localStorage.getItem('cart') == null) {
        cart = [];
        localStorage.setCart(cart);
    } else {
        cart = localStorage.getCart();
    }

    /*
     * Products page
     * Display the quantity according to local storage
     * Manage plus or minus button according to the products quantity
     */
    if (jQuery('.content.produits').length && cart != null) {
        for(i=0; i<cart.length; i++) {
            var numberInCart = localStorage.getProductNumberInCart(cart[i].id);
            var number = parseInt(jQuery('#number-'+cart[i].id).text());
            var newNumber = number-numberInCart;
            jQuery('#number-'+cart[i].id).text(newNumber);
            var quantity = parseInt(jQuery('#number-'+cart[i].id).text());
            if (numberInCart > 0) {
                console.log('numberInCart = '+numberInCart+' - .div-pm-to-cart-'+cart[i].id);
                jQuery('.add-to-cart-'+cart[i].id).css("display", "none");
                jQuery('.div-pm-to-cart-'+cart[i].id).css("display", "block");
                jQuery(".quantity-in-cart-"+cart[i].id).html(numberInCart);
                if (quantity == 0) {
                    jQuery('.div-pm-to-cart-'+cart[i].id+' >.plus-to-cart').addClass("disable");
                    jQuery('.div-pm-to-cart-'+cart[i].id+' >.plus-to-cart').attr("disabled", true);
                }
            }
        }
    }

    /*
     * Cart page
     * Add rows to the cart table according to local storage
     */ 
    if (jQuery('.content.panier').length) {
        if (cart.length > 0) {
            jQuery('.content.panier').append(
                '<table class="table">'+
                    '<thead>'+
                        '<tr>'+
                            '<td>Titre</td>'+
                            '<td>Prix</td>'+
                            '<td colspan="2">Quantité</td>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                        
                    '</tbody>'+
                '</table>'
            );
            for(i=0; i<cart.length; i++) {
                jQuery('.content.panier table tbody').append(
                    '<tr>'+
                        '<td>'+cart[i].title+'</td>'+
                        '<td>'+cart[i].price+'</td>'+
                        '<td id="number-'+cart[i].id+'">'+cart[i].number+'</td>'+
                        '<td><button class="small remove-from-cart" data-id="'+cart[i].id+'">Supprimer du panier</button></td>'+
                    '</tr>'
                );
            }
            jQuery('.content.panier').append(
                '<div>'+
                    '<button class="large validation">Valider ma commande</button>'+
                '</div>'
            );
        } else {
            jQuery('.content.panier').append(
                '<em>Votre panier est vide</em>'
            );
        }
    }

    /*
     * On click on add to cart button
     */
    jQuery('button.add-to-cart').on('click', function() {
        var id = jQuery(this).attr("data-id");
        var quantity = parseInt(jQuery('#number-'+id).text());
        if (quantity) {
            var title = jQuery(this).attr("data-title");
            var price = jQuery(this).attr("data-price");
            var product = {"id": id, "title": title, "price": price};
            localStorage.addProductToCart(product);
            var number = quantity-1;
            jQuery('#number-'+id).text(number);
            if (quantity-1 == 0) {
                jQuery(this).addClass("disable");
            } else {
                jQuery(this).removeClass("disable");
            }
            jQuery('.div-pm-to-cart-'+id).css("display", "block");
            jQuery('.add-to-cart-'+id).css("display", "none");
            jQuery(".quantity-in-cart-"+id).html('1');
        }
    });

    /*
     * On click on remove from cart button
     */
    jQuery('button.remove-from-cart').on('click', function() {
        var id = jQuery(this).attr("data-id");
        localStorage.removeProductFromCart(id);
        var number = parseInt(jQuery('#number-'+id).text());
        if (number == 1) {
            jQuery('#number-'+id).parent().fadeOut(300, function() {
                jQuery(this).remove();
            });
        } else {
            jQuery('#number-'+id).text(number-1);
        }
        
        // Nothing in the cart anymore
        if (localStorage.getCart().length <= 0) {
            jQuery('.content.panier > div').remove();
            jQuery('.content.panier > table').html('<em>Votre panier est vide</em>');
        }
    });

    /*
     * On click on cart validation button
     * Ajax request to cart-validation.php
     */
    jQuery('button.validation').on('click', function() {
        var cart = localStorage.getCart();
        jQuery.ajax({
            type: 'POST', // request type
            url: '/cart-validation.php', // data sent to cart-validation.php
            data: {
                cart: cart
            }, 
            // on success
            success: function(data, textStatus, jqXHR) {
                localStorage.clear();
                // notify user
                jQuery.pnotify({
                    text: data,
                    type: 'success'
                });
                jQuery('.content.panier > div').remove();
                jQuery('.content.panier > table').html('<em>Votre panier est vide</em>');
            },
            // on error
            error: function(jqXHR, textStatus, errorThrown) {
                // notify user
                jQuery.pnotify({
                    text: 'Une erreur est survenue',
                    type: 'error'
                });
            }
        });
    });

    /*
     * On click on plus button
     */
    jQuery('button.plus-to-cart').on('click', function() {
        // update number
        var id = jQuery(this).attr("data-id");
        var quantity = parseInt(jQuery('#number-'+id).text());
        var quantityInCart = parseInt(jQuery('.quantity-in-cart-'+id).text());
        if (quantity) {
            var title = jQuery(this).attr("data-title");
            var price = jQuery(this).attr("data-price");
            var product = {"id": id, "title": title, "price": price};
            localStorage.addProductToCart(product);
            var number = quantity-1;
            jQuery('#number-'+id).text(number);

        }
        quantityInCart = quantityInCart + 1;
        jQuery(".quantity-in-cart-"+id).html(quantityInCart);
        // disable button when there are not enough products
        if (quantity-1 == 0) {
            jQuery(this).addClass("disable");
            jQuery(this).attr("disabled", true);
        }
    });

    /*
     * On click on minus button
     */
    jQuery('button.minus-from-cart').on('click', function() {
        // update number
        var id = jQuery(this).attr("data-id");
        localStorage.removeProductFromCart(id);
        var number = parseInt(jQuery('#number-'+id).text());
        var quantityInCart = parseInt(jQuery('.quantity-in-cart-'+id).text());
        jQuery('#number-'+id).text(number+1);
        quantityInCart = quantityInCart - 1;
        jQuery(".quantity-in-cart-"+id).html(quantityInCart);

        // default button when quantity = 0
        if (quantityInCart == 0) {
            jQuery('.div-pm-to-cart-'+id).css("display", "none");
            jQuery('.add-to-cart-'+id).css("display", "block");
        }
        // enable button when there are still enough products
        if (number+1 > 0) {
            jQuery('button.plus-to-cart[data-id='+id+']').removeClass("disable");
            jQuery('button.plus-to-cart[data-id='+id+']').attr("disabled", false);
        }
    });
});
