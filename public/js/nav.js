jQuery('document').ready( function(){
	var positionElementInPage = jQuery('#nav').offset().top;
	$(window).scroll(
	    function() {
	        if (jQuery(window).scrollTop() >= positionElementInPage) {
	            // fixed
	            jQuery('#nav').addClass("floatable");
	            jQuery('body').css('margin-top', '40px');
	        } else {
	            // relative
	            jQuery('#nav').removeClass("floatable");
	            jQuery('body').css('margin-top', '0px');
	        }
	    }
	);
});