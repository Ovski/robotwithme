<?php

/**
 * Open a databse connection
 *
 * @return \PDO : a PDO instance
 */
function open_database_connection()
{
    try
    {
        $database = new PDO('mysql:host=localhost;dbname=plain_robots', 'plain_robots', 'plain_robots');
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }

    return $database;
}

/**
 * Get all robots in database
 *
 * @return array : all robots
 */
function get_all_robots()
{
    $database = open_database_connection();
    $answer = $database->query('SELECT id, title, price, number, imgbase64 FROM productlist');

    $robots = array();
    while ($row = $answer->fetch()) {
        if ($row['number'] > 0) {
            $robots[] = $row;
        }
    }

    return $robots;
}

/**
 * Update the number attribute of a robot
 *
 * @param decrementNumber : the quantity to remove from the initial number
 * @param id : the id of the concerned robot
 */
function update_robot_number($decrementNumber, $id)
{
    $database = open_database_connection();
    $request = $database->prepare('UPDATE productlist SET number = number - :number WHERE id = :id and number > 0');

    $request->execute(array(
        'number' => $decrementNumber,
        'id' => $id
    ));
}

/**
 * Insert a new user
 *
 * @param firstname : the user firstname
 * @param lastname : the user lastname
 * @param age : the user age
 * @param email : the user email
 */
function insert_user($firstname, $lastname, $age, $email)
{
    $database = open_database_connection();
    $request = $database->prepare('INSERT INTO registered_user (firstname, lastname, age, email)
        VALUES(:firstname, :lastname, :age, :email)');

    $request->execute(array(
        'firstname' => $firstname,
        'lastname' => $lastname,
        'age' => $age,
        'email' => $email
    ));
}

/**
 * Check whether or not a user exists
 *
 * @return boolean
 */
function user_exists($email)
{
    $database = open_database_connection();
    $answer = $database->query('SELECT id FROM registered_user WHERE email = \''.$email.'\'');

    while ($row = $answer->fetch()) {
        return true;
    }

    return false;
}




